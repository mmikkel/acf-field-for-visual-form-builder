<?php
/*
Plugin Name: Advanced Custom Fields: Visual Form Builder Field
Plugin URI: 
Description: Based on Gravity Forms Field by Adam Pope (@adam_pope)
Version: 1.0.0
Author: @matsmikkel (original code by @adam_pope of @stormuk)
Author URI: http://www.dekode.no
License: MIT
License URI: http://opensource.org/licenses/MIT
*/


class acf_field_visual_form_builder_plugin
{
	/*
	*  Construct
	*
	*  @description:
	*  @since: 3.6
	*  @created: 1/04/13
	*/

	function __construct()
	{

		// version 4+
		add_action('acf/register_fields', array($this, 'register_fields'));


		// version 3-
		add_action( 'init', array( $this, 'init' ), 5);
	}


	/*
	*  Init
	*
	*  @description:
	*  @since: 3.6
	*  @created: 1/04/13
	*/

	function init()
	{
		if(function_exists('register_field'))
		{
			register_field('acf_field_visual_form_builder', dirname(__File__) . '/visual_form_builder-v3.php');
		}
	}

	/*
	*  register_fields
	*
	*  @description:
	*  @since: 3.6
	*  @created: 1/04/13
	*/

	function register_fields()
	{
		include_once('visual_form_builder-v4.php');
	}

}

new acf_field_visual_form_builder_plugin();

?>